package first;

public class Car {

    public void start(){
        startElecticity();
        startCommand();
        startFuelSystem();
    }
    private void startElecticity(){
        System.out.println("Electicity on!");
    }
    private void startCommand(){
        System.out.println("Command started!");
    }
    private void startFuelSystem(){
        System.out.println("Fuel System on!");
    }
}
